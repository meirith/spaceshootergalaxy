﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cartridge : MonoBehaviour {

    public int maxAmmo = 20;
    public GameObject bulletPrefab;
    public Transform ammoTransform;
    public Bullet[] bullets;

    protected int currentBullet = 0;

    void Start ()
    {
        CreateBullets();
    }

    void CreateBullets()
    {
        bullets = new Bullet[maxAmmo];

        for(int i = 0; i < maxAmmo; i++)
        {
            Vector2 spawnPos = ammoTransform.position;
            spawnPos.x -= i * 0.2f;


            GameObject b = Instantiate(bulletPrefab, spawnPos, Quaternion.identity, ammoTransform);

            b.name = "Bullet_" + i;

            bullets[i] = b.GetComponent<Bullet>();
        }
      
    }
    public Bullet GetBullet()
    {
        Bullet b = bullets[currentBullet];

        
         currentBullet++;
        if (currentBullet >= maxAmmo) currentBullet = 0;

        return b;
             
    }
}
