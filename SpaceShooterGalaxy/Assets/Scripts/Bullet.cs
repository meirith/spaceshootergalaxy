﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet	 : MonoBehaviour {
	public float speed;
	public int damage;

	private bool shot;
	private Vector2 iniPos;
	// Use this for initialization
	void Start () {
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () {
		if(shot)
		{
			transform.Translate(Vector2.up * speed * Time.deltaTime);
		}

	}
	public void ShotBullet(Vector2 origin, float zRot)
	{
		shot = true;
		transform.position = origin;
        transform.rotation = Quaternion.Euler(0, 0, zRot);

	}
	private void Reset()
	{
		transform.position = iniPos;
		shot = false;
	}
	private void OnTriggerEnter2D(Collider2D other)
	{
		//Debug.Log(other.name);
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Boundary")
		{
			Reset();
		}
	}
}