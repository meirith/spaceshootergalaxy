﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	private PlayerBehaviour player;
	public ShotWeapon weapon;
   
    // Use this for initialization
    void Start () {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehaviour>();

	}

	// Update is called once per frame
	void Update ()
	{
		//InputPlayer
		Vector2 inputAxis = Vector2.zero;
		inputAxis.x = Input.GetAxis("Horizontal");
		inputAxis.y = Input.GetAxis("Vertical");

		player.SetAxis(inputAxis);

        //Input Cannon
        if(Input.GetButton("Fire1")) weapon.ShotWeapons();

        if(Input.GetKeyDown(KeyCode.E)) weapon.NextWeapon();
        if(Input.GetKeyDown(KeyCode.Q)) weapon.PreviewWeapon();
       

        //if(Input.GetButton("Fire1")) cannontriple.ShotCannon();





    }
}