﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleCannon : Canon
{


    public override void ShotCannon(Cartridge c)
    {
        if(!canShot) return;

        canShot = false;
        timeCounter = 0;
    
        c.GetBullet().ShotBullet(transform.position, 0);
        c.GetBullet().ShotBullet(transform.position,30);
        c.GetBullet().ShotBullet(transform.position,-30);



    }
}
