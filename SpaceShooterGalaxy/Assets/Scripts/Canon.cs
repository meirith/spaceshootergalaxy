﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour {

	/*
    public int maxAmmo=20;
	public GameObject bulletPrefab;
	public Transform ammoTransform;
	public Bullet[] bullets;        

    protected int currentBullet = 0;
    */

	public float cooldown =0.1f;
    protected float timeCounter;
	protected bool canShot =false;
	// Use this for initialization
	void Start() {
		//CreateBullets();
       
            }

	// Update is called once per frame
	void Update() {
		if(!canShot)
		{
			timeCounter += Time.deltaTime;
			if(timeCounter >= cooldown)
			{
				canShot = true;
			}
		}
	}
/*
 *void CreateBullets()
	{
		bullets = new Bullet[maxAmmo];

		for (int i = 0;i < maxAmmo; i++)
		{
			Vector2 spawnPos = ammoTransform.position;
			spawnPos.x -= i * 0.2f;
           

            GameObject b = Instantiate(bulletPrefab, spawnPos, Quaternion.identity, ammoTransform);
           
            b.name = "Bullet_" + i;

			bullets[i] = b.GetComponent<Bullet>();
		}
		canShot = true;
		timeCounter = 0;    }
   
    */

	public virtual void ShotCannon(Cartridge c)
	{
		if (!canShot) return;

		canShot = false;
		timeCounter = 0;

        c.GetBullet().ShotBullet(transform.position, 0);

        //bullets[currentBullet].ShotBullet(transform.position, 0);
        //  currentBullet++;
        //	if (currentBullet >= maxAmmo) currentBullet = 0;

    }
}
