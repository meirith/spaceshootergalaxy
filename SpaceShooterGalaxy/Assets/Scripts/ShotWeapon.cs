﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotWeapon : MonoBehaviour {

    public Canon[] cannons;
    public int currentCannon;
    public Cartridge cartidge;
	void Start ()
    {
        currentCannon = 0;
        cannons = GetComponentsInChildren<Canon>();

    }


    public void ShotWeapons ()
    {
        cannons[currentCannon].ShotCannon(cartidge);
	}

    public void NextWeapon()
    {
        currentCannon++;

        if(currentCannon >= cannons.Length)
        {
            currentCannon = 0;
        }
    }

    public void PreviewWeapon()
    {
        currentCannon--;
        if(currentCannon <0)
        {
            currentCannon =0;
        }
    }
}
